package com.intimetec.week1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar.OnRatingBarChangeListener
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_example.*


class ExampleFragment : Fragment() {

    val YES = 0
    val NO = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_example, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        radio_group.setOnCheckedChangeListener { _, checkedId ->
            val radioButton =
                radio_group.findViewById<View>(checkedId)
            val index = radio_group.indexOfChild(radioButton)
            val textView: TextView = activity!!.findViewById(R.id.fragment_header)
            when (index) {
                YES -> {
                    textView.setText(R.string.yes_message)
                    fragment_header2_layout.visibility = View.VISIBLE
                }
                NO -> {
                    textView.setText(R.string.no_message)
                    fragment_header2_layout.visibility = View.GONE
                }
                else -> {
                }
            }
        }

        ratingBar.onRatingBarChangeListener =
            OnRatingBarChangeListener { ratingBar, _, _ ->
                val myRating =
                    getString(R.string.my_rating) + ratingBar.rating.toString()
                if (fragment_header2_layout.visibility == View.VISIBLE) {
                    Toast.makeText(
                        context, myRating,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

    }
}
